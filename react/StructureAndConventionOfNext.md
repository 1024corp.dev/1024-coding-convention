### <center>Structure and Convention Of Next</center>





#### 기본 작성 규칙

------

- 모든 directory 와 file 명칭은 명사형으로 작성한다.

* 모든 directory 와 file 명칭은 별도의 요구 사항이 없는 한 camel case 로 작성한다.

  > UserInfo (x) , userinfo (x) , Userinfo (x) -> userInfo (o)

* 다수의  파일이 들어가는 directory 는 기본적으로 복수형으로 작성한다.

  > store (x) -> stores (o) , interface (x) -> interfaces (o)

* Component directory 인 경우에는 반드시 단수형으로 작성한다.

  > Items (x) -> Item , Buttons (x) -> Button (o)

- 모든 데이터 타입은 interface 를 기본으로 사용한다. (단, interface 에서 지원하지 않는 기능을 사용해야 하는 경우에 한해서 type 을 사용할 수 있다)

- 전역 state 는 [zustand](https://github.com/pmndrs/zustand) 를 기본으로 사용한다.

- 모든 variable (parameter 포함) 는 반드시 camel case 를 적용하며, 아래의 세부 규칙을 지켜서 작성한다.

  - 모든 variable 은 기본적으로 명사형으로 작성한다.

    > userData , errorMessage , userName

  - Boolean 타입의 변수인 경우에는 동사 is 와 과거분사 형태의 동사, 진행형 형태의 동사 또는 명사를 결합해서 사용한다.

    > is + 과거 분사 형태
    >
    > ```typescript
    > let isOpend = false;
    > let isClosed = true;
    > ```
    >
    > is + 진행형 형태
    >
    > ```typescript
    > let isLoading = true;
    > let isChecking = true;
    > ```
    >
    > is + 명사
    >
    > ```typescript
    > let isError = true;
    > let isData = false;
    > ```

- 모든 method (function) 는 반드시 camel case 를 적용하며, 아래의 세부 규칙을 지켜서 작성한다.

  - 모든 method (function) 는 반드시 arrow function 형태로 작성한다.

    > ```typescript
    > // 잘못된 예 (function 사용)
    > 
    > function getData() {
    >   	// Do something
    > }
    > ```

  - 모든 method (function) 는 기본적으로 동사와 목적어가 결합된 방식으로 작성한다.

    > ```typescript
    > const clickItem = () => {
    >   	// Do something
    > }
    > ```

  - 자세한 설명이 필요한 경우에는 to 부정사 형태를 이용하여 작성한다. ( 동사<목적어> + to + 동사<목적어>)

    > ```typescript
    > const clickButtonToSubmit = () => {
    >   	// Do something
    > }
    > ```
    >
    > ```typescript
    > const clickIconToShowImage = () => {
    >   	// Do something
    > }
    > ```

- 모든 상수 (canstant) 는 반드시 대문자만 사용해야  하며, 아래의 세부 규칙을 지켜서 작성한다.

  - 모든 constant 는 snake case 로 작성한다.

    > ```typescript
    > // 개별적인 상수를 정의하는 경우
    > 
    > const STATUS = 200;
    > const REQUEST_TYPE_GET = "get";
    > ```
    >
    > ```typescript
    > // object 방식으로 상수를 정의하는 경우
    > 
    > const DIRECTION = {
    >     UP: 0,
    >     RIGHT: 1,
    >     DOWN: 2,
    >     LEFT: 3
    > }
    > ```
    >
    > ```typescript
    > // enum 방식으로 상수를 정의하는 경우
    > 
    > enum DIRECTION {
    >     UP,
    >     RIGHT,
    >     DOWN,
    >     LEFT 
    > }
    > ```





#### Directory 구조 및 세부 작성 규칙

------

##### [ pages ]

- Next.js frameWork 의 기본 directory 이며, 절대 이름을 변경하거나 삭제하지 않는다.

- 실제 url 에 의해서 호출되는 file 을 보관하여, 기본적인 작성 규칙은 아래의 세부 규칙을 따른다.

  - Url 및 get 요청에  대한 parameter 를 정의할 수 있으므로 , next.js 의 기본 규칙을 충분히 숙지한다.

  - 여러 개의 단어로 구성되는 파일 명칭은 snake case 로 작성한다. (url 로 호출)

    > mypage (x) , myPage (x) , MyPage (x) , my-page (x) -> my_page

  - [ api ] directory 는 Next.js frameWork 의 기본 directory 이며, 절대 이름을 변경하거나 삭제하지 않는다. (실제 호출되는 API 작성)

  - [ api ] 하위에 dummy 데이터 테스트를 위해서 다음과 같이 하위 directory 를 구성한다.

    > pages
    >
    > 　　⎿ api
    >
    > 　　　　⎿ dummies

  - [ api ] directory 의 file 명칭은 snake case 로 작성하고, 실제 반환하는 function 은 camel case 로 작성한다.

    > pages
    >
    > 　　⎿ api
    >
    > 　　　　⎿ dummies
    >
    > 　　　　　　⎿ get_user.ts
    >
    > ```typescript
    > // file 명칭이 get_user 인 경우
    > 
    > const getUser = (req: NextApiRequest, res: NextApiResponse) => {
    >     // Do something
    >     res.status(200).json();
    > }
    > ```



##### [ public ]

- Next.js frameWork 의 기본 directory 이며, 절대 이름을 변경하거나 삭제하지 않는다.

- Static file 을 보관하며, 아래와 같은 하위 directory 를 가질 수 있다. (필요한 경우 자유롭게 directory  구조를 확장할 수 있다)
  - images  : 프로젝트에 사용되는 static 이미지를 저장한다.
  - htmls : 프로젝트에 사용되는 static html 문서를 저장한다.



##### [ components ]

- 프로젝트 내에 사용되는 모든 component 를 보관하는 directory 이다.

- 하위 component 를 보관하는 경로인 경우에는 [ _directory명 ] 으로 작성한다.

  > _common , _modals , _items

- 실제 component 를 보관하는 directory 인 경우에는 아래와 같은 세부 규칙을 따른다.

  - Component directory 에는 component 파일, style 파일, interface 파일을 기본 구성으로 한다.

  - Component 파일의 경우에는 반드시 index.tsx 를 파일의 기본 명칭으로 한다.

    > components
    >
    > 　　⎿ userInfo
    >
    > 　　　　⎿ index.tsx
    
  -  실제 반환하는 component 함수는 component directory 명칭과 동일해야 하며, 반드시 pascal case 형식으로 작성한다.
  
    > ```react
    > // Component directory 명이 userInfo 인 경우
    > 
    > const UserInfo = () => {
    >   	return (<></>);
    > }
    > 
    > export default UserInfo;
    > ```

  - 개별 component 내부에 자식 component 가 필요한  경우, 위에 언급한 component 네이밍 규칙과 동일한 규칙을 적용해서 작성할 수 있다. (단, 자식 component 는 부모  component 외에 다른 곳에서 절대 사용되어서는 안된다)

    > components
    >
    > 　　⎿ userInfo
    >
    > 　　　　⎿ index.tsx
    >
    > 　　　　⎿ userInfo.interface.ts
    >
    > 　　　　⎿ userInfo.module.scss
    >
    > 　　　　⎿ userInfoDetail
    >
    > 　　　　　　⎿ index.tsx
    >
    > 　　　　　　⎿ userInfoDetail.interface.ts
    >
    > 　　　　⎿ userFriend
    >
    > 　　　　　　⎿ index.tsx
    >
    > 　　　　　　⎿ userFriend.style.ts
  
  - Style 파일의 경우에는 반드시 module scss 를 사용하고, 명칭은 directory 명칭과 동일하게 작성한다.
  
    > component
    >
    > 　　⎿ userInfo
    >
    > 　　　　⎿ userInfo.module.scss
  
  - Javascript 기반으로 css 를 작성하는 library (emotion) 를 사용하는 경우에는 file 명에 ".style" surfix 를 붙여서 작성한다. (style 을 별도의 파일로 분기하는 경우에만 해당한다.)
  
    > component
    >
    > 　　⎿ userInfo
    >
    > 　　　　⎿ userInfo.style.ts
  
  - Interface 파일의 경우에는 directory 명칭과 동일해야 하며, 반드시 ".interface" surfix 를 붙여서 작성한다. 
  
    > components
    >
    > 　　⎿ userInfo
    >
    > 　　　　⎿ userInfo.interface.tsx
  
  - 실제 반환하는 interface 는 component directory 명칭과 동일해야 하며, 반드시 대문자 "I" 를 prefix 로 붙이고 pascal case 형식으로 작성한다.
  
    > ``` typescript
    > // Component directory 명이 userInfo 인 경우
    > 
    > interface IUserInfo {
    >     id: number,
    >     name: string
    > }
    > 
    > export default IUserInfo;
    > ```
  
  - Interface 파일의 경우에는 해당 component 에서만 사용되는 interface 를 정의 해야하며, 여러 개의 interface 를 정의해서 export 할 수 있다.  (단, 해당 componet 외에 다른 곳에서는 절대 사용되지 않아야 한다)
  
    > ```typescript
    > // 한 개의 interface 만 반환하는 경우
    > 
    > interface IUserInfo {
    >     id: number,
    >     name: string
    > }
    > 
    > export default IUserInfo;
    > ```
    >
    > ```typescript
    > // 여러 개의 interface 를 반환하는 경우
    > 
    > interface IUserInfo {
    >     id: number,
    >     name: string
    > }
    > 
    > interface IUserInfoDetail {
    >     money: number,
    >     nickName: string
    > }
    > 
    > export type { IUserInfo, IUserInfoDetail }
    > ```



##### [ hooks ]

- 프로젝트 내에 사용되는 모든 custom hook 을 보관하는 directory 이다.

- 하위 hook 을 보관하는 경로인 경우에는 반드시 "Hooks" surfix 를 붙여서 작성한다.

  > userHooks , inputHooks

- 모든 hook file 의 명칭은 반드시 ".hook" surfix 를 붙여서 작성한다.

  > hooks
  >
  > 　　⎿ formHooks
  >
  > 　　　　⎿ form.hook.ts
  >
  > 　　⎿ modalController.hook.ts
  
- 실제 번환하는 hook 의 명칭에는 "use" prefix 를 붙이는 React 기본 규칙을 반드시 적용해야 한다.

  > ```typescript
  > // form.hook.ts
  > 
  > const useForm = () => {
  >   	// Do something
  > }
  > 
  > export default useForm;
  > ```



##### [ styles ]

- 프로젝트 내에 공통으로 사용되는 style file (scss) 을 보관하는 directory 이다.

- 하위 style 을 보관하는 경로인 경우에는 복수형으로 작성한다. (복수형이 존재하지 않거나, 단어 자체가 복수 의미를 포함하는 경우는 예외)

  > variables , mixin



##### [ interfaces ]

- 프로젝트 내에 공통으로 사용되는 interface file 을 보관하는 directory 이다. (단, component 내에서 사용되는 interface 는 componet directory 에 보관한다)

- 하위 interface 를 보관하는 경로인 경우에는 반드시 "Interfaces" surfix 를 붙여서 작성한다.

  > hookInterfaces , constantInterfaces

- 모든 interface file 명칭은 반드시 ".interface" surfix 를 붙여서 작성한다.

  > interfaces
  >
  > 　　⎿ hookInterfaces
  >
  > 　　　　⎿ checkBox.interface.ts
  >
  > 　　⎿ axiosInterceptorResponse.interface.ts

- 1개의 파일에는 1개의 interface 만 작성하고, default 로 export 하는 것을 기본 원칙으로 한다. (단, 다른 interface 에서 상속 받거나 componet, hook, page 등에서 독립적으로 사용되는 경우가 없고, default 로 export 하는 interface 의 일부인 경우에는 예외로 인정한다)

  > ```typescript
  > // IFriends interface 가 IUserInfo interface 의 일부로 사용되고 다른 곳에서는 사용되지 않는 경우
  > 
  > interface IFriends {
  >     name: string;
  >     age: number;
  > }
  > 
  > interface IUserInfo {
  >     id: number;
  >     name: string;
  >     friends: IFriends[]
  > }
  > 
  > export default IUserInfo;
  > ```



##### [ utils ]

- 프로젝트 내에서 공통으로 사용되는 function file 을 보관하는 directory 이다.

- 하위 function 을 보관하는 경로인 경우에는 반드시 "Utils" surfix 를 붙여서 작성한다.

  > dateUtils , validationUtils

- 모든 util file 명칭은 반드시 ".util" surfix 를 붙여서 작성한다.

  >utils
  >
  >　　⎿ validationUtils
  >
  >　　　　⎿ userIdChecker.util.ts
  >
  >　　⎿ userNameConverter.utils.ts

- 1개의 파일에는 1개의 function 만 작성하고, default 로 export 하는 것을 기본 원칙으로 한다.

  > ```typescript
  > const validateUserPhoneNumber = () => {
  >   	// Do something
  > }
  > 
  > export default validateUserPhoneNumber;
  > ```

- 1개의 파일에 여러 개의 function 을 작성할  경우에는 서로 밀접한 관련이 있는 기능을 수행해야  하며, class 또는 object 로 작성한다.

  - Object 로 작성하는 경우

  > ```typescript
  > const userValidator = {
  >     checkName: (name: string) => {
  >        // Do something
  >     },
  >     checkPhoneNumber: (phoneNumber: string) => {
  >       // Do something
  >     }
  > }
  > 
  > export default validateUser;
  > ```

  - Class 로 작성하는 경우 (class 로 작성할 경우에는 반드시 pascal case 를 적용한다)

  > ```typescript
  > // 생성자를 활용해야 하는 경우에는 class 로 작성하는 것을 권장
  > 
  > class userValidator {  
  >     private _user: User;
  > 
  >     constructor(user: User) {
  >        this._user = user;
  >     }
  > 
  >     public checkName = () => {
  >        // Do something
  >     }
  > 
  >     public checkPhoneNumber = () => {
  >        // Do something
  >     }
  > }
  > 
  > export default ValidateUser;
  > ```



##### [ stores ]

- 프로젝트 내에서 사용되는 전역 state file 을 보관하기 위한 directory  이다.

-  하위 state 를 보관하는 경로인 경우에는 반드시 "Stores" surfix  를 붙여서 작성한다.

  > authenticationStores , paymentStores

- 모든 store 명칭은 반드시 ".store" surfix 를 붙여서 작성한다.

  > stores
  >
  > 　　⎿ authenticationStores
  >
  > 　　　　⎿ authentication.store.ts
  >
  > 　　⎿ user.store.ts

- 1개의 파일에는 반드시 1개의 전역 state 만 작성하고, default 로 export 하는 것을 원칙으로 한다.



##### [ constants ]

- 프로젝트 내에서 사용되는 상수를 보관하기 위한 directory 이다.

- constants directory 는 하위 directory 를 허용하지 않는다.

- 실제 반환하는 constant 는 반드시 object 형태로 작성해야 하며, default 로 export 하는 것을 원칙으로 한다.

  > ```typescript
  > // object 형식의 상수
  > 
  > const HTTP_STATUS = {
  >     DEFAULT: -1,
  >     BAD_REQUEST: 400,
  >     NOT_FOUND: 404,
  >     INTERNAL_SERVER_ERROR: 500
  > }
  > 
  > export default STATUS;
  > ```
  >
  > ```typescript
  > // enum 형식의 상수
  > 
  > enum HTTP_STATUS {
  >     DEFAULT = -1,
  >     BAD_REQUEST = 400,
  >     NOT_FOUND = 404,
  >     INTERNAL_SERVER_ERROR = 500
  > }
  > 
  > export default HTTP_STATUS;
  > ```

- file 명칭은 반환하는 constant 와 동일하게 작성하되, camel case 로 작성한다.

  > 위의 예시를 반환하는 경우에는 httpStatus.ts 로 작성한다.



##### [ data ]

- 프로젝트 내에서 사용되는 text 형태의 static 테이터를 보관하기 위한 directory 이다.

- data directory 는 하위 directory 를 허용하지 않는다.

- 실제 반환하는 data 는 반드시 object (json) 형태로 작성해야 하며, default 로 export 하는 것을 원칙으로 한다.

  > ```typescript
  > const selectionItems = [
  >     {
  >       id: 1,
  >       name: "곰 인형"
  >     },
  >     {
  >       id: 2,
  >       name: "고양이 인형"
  >     }
  > ];
  > 
  > export default selectionItems;
  > ```

- file 명칭은 반환하는 data object 와 동일하게 작성한다.

  > 위의 예시를 반환하는 경우에는 selectionItems.ts 로  작성한다.



##### [ httpModules ]

- http 통신을 위한 기본 library 와 dummy data 를 보관하기 위한 directory 이다.

- [ dummyData ] 는 [ httpModules ] 의 하위 directory 로서 클라이언트 테스트용 데이터를 보관하며, 아래와 같은 세부 작성 규칙을 따른다.

  - 실제 반환하는 data 는 http 통신을 통해서 받아오는 data 와 동일한 interface 를 적용해야 한다.

  - 실제 반환하는 object 명칭은 [ pages ] -> [ api ] -> [ dummies ] 하위에 있는 file 의 method 명칭과 동일해야 하며, "data_" prefix 를 붙인다.

    > ```typescript
    > // file 경로 : pages -> api -> dummies -> get_user.ts
    > // 반환하는 method 명칭 : getUser
    > 
    > const data_getUser = {
    >   	// Make dummy data
    > }
    > 
    > export default data_getUser;
    > ```

