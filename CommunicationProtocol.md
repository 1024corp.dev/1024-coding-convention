### <center>Protocol between front-end and back-end</center>



#### 기본 구조

* Javascript 언어의 Json 문법에 맞추어 작성한다.
* Key 명칭은 camel case 형식에 맞추어 작성한다.

```typescript
{
  processedDateTime: number,
  metaData: {} | null,
  data: {} | [] | null
}
```



#### 항목 설명

1. processedDateTime

   서버에서 처리된 시간이며, 반드시 unix time stamp 로 작성한다.

2. metaData

   data 항목 사용에 필요한 부가 정보를 작성하며, 필요하지 않을 경우에는 null 로 작성한다.

   pagination 을 위한 정보들이 대표적인 사용 예이다.

3. data

   front-end 에서 사용하는 실제 데이터를 작성하며, 데이터가 없는 경우에는 null 로 작성한다.

   반드시 실제 데이터만 있어야 하며, 그 형식은 object 또는 array 만 허용된다.



#### 사용 예

1. metaData 가 있는 경우

   ```javascript
   {
     processedDateTime: 1657669338,
     metaData: {
       allCount: 234,
       currentPage: 1
     },
     data: [
       {
         id: 1,
         name: "user1"
       },
       {
         id: 2,
         name: "user2"
       },
       {
         id: 3,
         name: "user3"
       }
     ]
   }
   ```

2. metaData 가 없는 경우

   ```javascript
   {
     processedDateTime: 1657669338,
     metaData: null,
     data: {
       goodsId: 2423,
       goodsName: "곰인형",
       goodsPrice: 23000
     }
   }
   ```

3. data 가 없는 경우

   ```javascript
   {
     processedDateTime: 1657669338,
     metaData: null,
     data: null
   }
   ```



